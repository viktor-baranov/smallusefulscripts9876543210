// Insert in the dev-console of the new browser GB

(function(console) {
    console.save = function(data, filename) {
        if (!data) {
            console.error('Console.save: No data')
            return;
        }
        if (!filename) filename = 'console.json'
        if (typeof data === "object") {
            data = JSON.stringify(data, undefined, 4)
        }
        var blob = new Blob([data], {
                type: 'text/json'
            }),
            e = document.createEvent('MouseEvents'),
            a = document.createElement('a')
        a.download = filename
        a.href = window.URL.createObjectURL(blob)
        a.dataset.downloadurl = ['text/json', a.download, a.href].join(':')
        e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
        a.dispatchEvent(e)
    }
})(console);
var savedData = '\n\n';
savedData += ' == learnerAss == ';
savedData += '\n\n';
savedData += (learnerAss !== undefined) ? JSON.stringify(learnerAss) : '_is_undefined';
savedData += '\n\n';
savedData += ' == learnerAssMap == ';
savedData += '\n\n';
savedData += (learnerAssMap !== undefined) ? JSON.stringify(learnerAssMap) : '_is_undefined';
savedData += '\n\n';
savedData += ' == isEnabledWeightageAo == ';
savedData += '\n\n';
savedData += (isEnabledWeightageAo !== undefined) ? JSON.stringify(isEnabledWeightageAo) : '_is_undefined';
savedData += '\n\n';
savedData += ' == learningPlans == ';
savedData += '\n\n';
savedData += (learningPlans !== undefined) ? JSON.stringify(learningPlans) : '_is_undefined';
savedData += '\n\n';
savedData += ' == helpPlans == ';
savedData += '\n\n';
savedData += (helpPlans !== undefined) ? JSON.stringify(helpPlans) : '_is_undefined';
savedData += '\n\n';
savedData += ' == contactPlans == ';
savedData += '\n\n';
savedData += (contactPlans !== undefined) ? JSON.stringify(contactPlans) : '_is_undefined';
savedData += '\n\n';
savedData += ' == offering == ';
savedData += '\n\n';
savedData += (offering !== undefined) ? JSON.stringify(offering) : '_is_undefined';
savedData += '\n\n';
savedData += ' == weightCategories == ';
savedData += '\n\n';
savedData += (weightCategories !== undefined) ? JSON.stringify(weightCategories) : '_is_undefined';
savedData += '\n\n';
savedData += ' == planTypes == ';
savedData += '\n\n';
savedData += (planTypes !== undefined) ? JSON.stringify(planTypes) : '_is_undefined';
savedData += '\n\n';
savedData += ' == plansMap == ';
savedData += '\n\n';
savedData += (plansMap !== undefined) ? JSON.stringify(plansMap) : '_is_undefined';
savedData += '\n\n';
savedData += ' == studentsShown == ';
savedData += '\n\n';
savedData += (studentsShown !== undefined) ? JSON.stringify(studentsShown) : '_is_undefined';
savedData += '\n\n';

console.save(savedData);
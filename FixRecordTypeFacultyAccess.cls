// Script to fix of record type on object Faculty_Access__c from SF-dev-console (Setting the record type to 'Department')

final Id DEPARTMENT_TYPE_ID = SObjectType.motivis_lrm__Faculty_Access__c.getRecordTypeInfosByName().get('Department').getRecordTypeId();
Savepoint sp = Database.setSavepoint();
try {
	List<motivis_lrm__Faculty_Access__c> facultyAccesses = [
		SELECT RecordTypeId
		FROM motivis_lrm__Faculty_Access__c
		WHERE RecordTypeId != :DEPARTMENT_TYPE_ID
		LIMIT 10000
	];
	if (!facultyAccesses.isEmpty()) {
		for (motivis_lrm__Faculty_Access__c facultyAccess : facultyAccesses) {
			facultyAccess.RecordTypeId = DEPARTMENT_TYPE_ID;
		}
		update facultyAccesses;
		System.debug('Updated ' + facultyAccesses.size() + ' records.');
	} else {
		System.debug('No records for update.');
	}
} catch(Exception ex) {
	Database.rollback(sp);
	System.debug('Update failed');
	System.debug(ex);
}